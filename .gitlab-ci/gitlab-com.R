# check if this is gitlab.com
install.packages("remotes", repos = "https://cloud.r-project.org/")
remotes::install_git("https://gitlab.com/fvafrCU/fritools")
r <- fritools::is_running_on_gitlab_com(verbose = TRUE)
warning(attr(r, "message"))
print(r)
if (!isTRUE(r)) {
    stop("fritools: Do not recognize gitlab.com") 
} else {
    message("Node is ", Sys.info()[["nodename"]],
            ", I guess this is gitlab.com.")
}
