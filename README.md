[![pipeline status](https://gitlab.com/fvafrCU/HandTill2001/badges/master/pipeline.svg)](https://gitlab.com/fvafrCU/HandTill2001/commits/master)    
[![coverage report](https://gitlab.com/fvafrCU/HandTill2001/badges/master/coverage.svg)](https://gitlab.com/fvafrCU/HandTill2001/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrCU/HandTill2001.svg?branch=master)](https://travis-ci.org/fvafrCU/HandTill2001)
    [![Coverage Status](https://codecov.io/github/fvafrCU/HandTill2001/coverage.svg?branch=master)](https://codecov.io/github/fvafrCU/HandTill2001?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/HandTill2001)](https://cran.r-project.org/package=HandTill2001)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/HandTill2001)](https://cran.r-project.org/package=HandTill2001)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/HandTill2001)](https://cran.r-project.org/package=HandTill2001)

<!-- README.md is generated from README.Rmd. Please edit that file -->



# HandTill2001
## Introduction
Please read the
[vignette](https://CRAN.R-project.org/package=HandTill2001/vignettes/consensus_auc.pdf).

Or, after installation, the help page:

```r
help("HandTill2001-package", package = "HandTill2001")
```

```
#> Multiple Class Area under ROC Curve
#> 
#> Description:
#> 
#>      A very lean package implementing merely M given by Hand and Till
#>      (2001), Eq. (7).
#> 
#> Details:
#> 
#>      M given by Hand and Till (2001) defines a multiple class version
#>      of the area under curve of the receiver operating characteristic.
#> 
#> References:
#> 
#>      David J. Hand and Robert J. Till (2001). A Simple Generalisation
#>      of the Area Under the ROC Curve for Multiple Class Classification
#>      Problems. _Machine Learning_ *45*(2), p. 171-186. DOI:
#>      10.1023/A:1010920819831.
#> 
#> See Also:
#> 
#>      'help(package="HandTill2001")', especially '?HandTill2001::auc';
#>      various packages that calculate binary class AUC ('ROCR') or
#>      multiple class AUC (pROC, 'caTools').
#> 
#> Examples:
#> 
#>      library(HandTill2001)
#>      data(ht01.multipleclass)
#>      auc( 
#>        multcap(
#>           response = ht01.multipleclass$observed
#>          , predicted = as.matrix(ht01.multipleclass[, levels(ht01.multipleclass$observed)])
#>          )
#>          )
#> 
```

## Installation

You can install HandTill2001 from gitlab via:


```r
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("fvafrCU/HandTill2001")
```


