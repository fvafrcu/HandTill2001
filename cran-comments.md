Dear CRAN Team,
this is a resubmission of package 'HandTill2001'. I have added the following changes:

* Add `ROCR` and `caTools` to the Depends-Field in file DESCRIPTION.

Please upload to CRAN.
Best, Andreas Dominik

# Package HandTill2001 1.0.0.9000

Reporting is done by packager version 1.5.0

## Test  environments 
- R Under development (unstable) (2020-09-23 r79248)
    Platform: x86_64-pc-linux-gnu (64-bit)
    Running under: Devuan GNU/Linux 3 (beowulf)
    0 errors | 0 warnings | 1 note 
- win-builder (devel)

## Local test results
- RUnit:
    HandTill2001_unit_test - 1 test function, 0 errors, 0 failures in 1 checks.
- Testthat:
    OK: 1, Failed: 0, Warnings: 1, Skipped: 0
- Coverage by covr:
    HandTill2001 Coverage: 100.00%

## Local meta results
- Cyclocomp:
     no issues.
- lintr:
    found 144 lints in 462 lines of code (a ratio of 0.3117).
- cleanr:
    found 4 dreadful things about your code.
- codetools::checkUsagePackage:
    found 3 issues.
- devtools::spell_check:
    found 21 unkown words.
