# Platform

|field    |value                                              |
|:--------|:--------------------------------------------------|
|version  |R Under development (unstable) (2019-12-18 r77593) |
|os       |Devuan GNU/Linux ascii                             |
|system   |x86_64, linux-gnu                                  |
|ui       |X11                                                |
|language |en_US:en                                           |
|collate  |en_US.UTF-8                                        |
|ctype    |en_US.UTF-8                                        |
|tz       |Europe/Berlin                                      |
|date     |2019-12-19                                         |

# Dependencies

|package      |old    |new   |Δ  |
|:------------|:------|:-----|:--|
|HandTill2001 |0.2-12 |1.0.0 |*  |

# Revdeps

## Failed to check (1)

|package  |version |error |warning |note |
|:--------|:-------|:-----|:-------|:----|
|ModelMap |?       |      |        |     |

